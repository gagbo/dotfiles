if test -r "$HOME/.guix-profile"
  set -gx GUIX_LOCPATH $HOME/.guix-profile/lib/locale
  set -gx GUIX_PROFILE $HOME/.guix-profile
  set -gx PATH $HOME/.guix-profile/bin $PATH
  set -gx PATH $HOME/.config/guix/current/bin $PATH
  set -gx GUILE_LOAD_PATH $GUIX_PROFILE/share/guile/site/3.0 $GUILE_LOAD_PATH
  set -gx GUILE_LOAD_COMPILED_PATH $GUIX_PROFILE/lib/guile/3.0/site-ccache $GUIX_PROFILE/share/guile/site/3.0 $GUILE_LOAD_COMPILED_PATH
end
