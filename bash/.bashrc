#vim: filetype=bash
# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
        . /etc/bashrc
fi

# Standard completion
# Try /etc (Linux) /usr/local/etc (BSDs) and brew --prefix (MacOS) in order
if [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
elif [ -f /etc/profile.d/bash_completion.sh ]; then
    . /etc/profile.d/bash_completion.sh
elif [ -f /usr/local/etc/bash_completion ]; then
    . /usr/local/etc/bash_completion
elif command -v "brew" > /dev/null 2>&1 && \
	 [ -f "$(brew --prefix)/etc/bash_completion.d/git-completion.bash" ]; then
    . $(brew --prefix)/etc/bash_completion.d/git-completion.bash
fi

# Weboob completion
if [ -f "${HOME}/soft/weboob-devel/tools/weboob_bash_completion" ]; then
        . "${HOME}/soft/weboob-devel/tools/weboob_bash_completion"
fi

# Rust completion
if [ -f "${HOME}/.config/rustup.bash_completion" ]; then
        . "${HOME}/.config/rustup.bash-completion"
fi

# FZF bindings
if [ -f /usr/share/fzf/shell/key-bindings.bash ]; then
        . /usr/share/fzf/shell/key-bindings.bash
fi
# Aliases
if [[ -r ~/.aliasrc ]]; then
    . ~/.aliasrc
fi


# Prompt Magic
function __ps_powerline() {
    echo "$(powerline-go -error $?)"
}

function __ps_git() {
    if ! declare -f -F __git_ps1 >/dev/null; then
	return
    fi

    local state="$(__git_ps1 '(%s)')"

    # if [[ ! -z $state ]]; then
    # 	local hash=$(git rev-parse --verify --short HEAD 2>/dev/null)
    # 	state=$(echo "$state" | sed "s/)/ $hash)/" | sed "s/\s*)/)/")
    # fi

    echo "$state"

}

function __ps_svn() {
    local path name rev

    if svn info >/dev/null 2>&1; then
	path="$(svn info | grep '^URL:' | egrep -o '(tags|branches)/[^/]+|trunk' | sed -e 's|^tags/\(.\+\)$|(\1)|')"
	rev="$(svn info | awk '/Revision:/ {print $2}')"

	if [ -z "$path" ]; then
	    path="$(svn info | grep '^Repository Root:')"
	fi

	name="$(echo $path | egrep -o '[^/]+$')"
    fi

    if [ ! -z "$name" ]; then
	echo -n " (svn $name $rev)"
    fi

}

function __ps_aws() {
    [ $AWS_CONFIG_FILE ] && echo " (aws ${AWS_DEFAULT_PROFILE:-default})"

}

# Custom bash prompt via kirsle.net/wizards/ps1.html
# The PS1 is separated by "set color + component" lines
function __ps1_prompt_cmd_before_git() {
    local prompt=""
    prompt="\[$(tput setaf 3)\]\t "
    prompt+="\[$(tput setaf 7)\]["
    if [ $(id -u) = 0 ]; then
	prompt+="\[$(tput setaf 1)\]\u"
    else
	prompt+="\[$(tput setaf 4)\]\u"
    fi
    prompt+="\[$(tput setaf 7)\]@"
    prompt+="\[$(tput setaf 6)\]\h "
    prompt+="\[$(tput setaf 7)\]\w]"
    prompt+="\[$(tput sgr0)\]"
    echo "$prompt"
}

function __ps1_prompt_cmd_after_git() {
    local prompt=""
    prompt+="\[$(tput sgr0)\]"
    prompt+="\[$(tput setaf 7)\] \$(__ps_svn)"
    prompt+="\[$(tput setaf 3)\]\$(__ps_aws)"
    prompt+="\n"
    prompt+="\[$(tput setaf 4)\](\j)\\$ "
    prompt+="\[$(tput sgr0)\]"
    echo "$prompt"
}

function __ps1_prompt() {
    local prompt=""
    prompt="$(__ps1_prompt_cmd_before_git)"
    prompt+="$(__ps1_prompt_cmd_after_git)"
    echo "$prompt"
}

export GIT_PS1_SHOWCOLORHINTS=true
export GIT_PS1_SHOWDIRTYSTATE=true
export GIT_PS1_SHOWSTASHSTATE=true
export GIT_PS1_SHOWUNTRACKEDFILES=true
export GIT_PS1_SHOWUPSTREAM="verbose"
export GIT_PS1_DESCRIBE_STYLE="default"
export GIT_PS1_STATESEPARATOR="|"
if ! declare -f -F __git_ps1 >/dev/null; then
    PS1="$(__ps1_prompt)"
    export PS1
else
    PROMPT_COMMAND='__git_ps1 "$(__ps1_prompt_cmd_before_git)" "$(__ps1_prompt_cmd_after_git)"'
    export PROMPT_COMMAND
fi

if [[ $TERM = xterm* ]] || [[ $TERM = konsole* ]]; then
    PS1="${PS1}\[\033]0;\W\007\]" # Changes the title of the window
    export PS1
fi

# Start fish if bash was only invoked without arguments
if command -v "fish" > /dev/null 2>&1; then
    if [ -z "$BASH_EXECUTION_STRING" ] && [[ $TERM != 'eterm-color' ]]; then
	true
    fi
fi

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/home/gagbo/miniconda3/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/home/gagbo/miniconda3/etc/profile.d/conda.sh" ]; then
        . "/home/gagbo/miniconda3/etc/profile.d/conda.sh"
    else
        export PATH="/home/gagbo/miniconda3/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<


PATH="/home/gagbo/perl5/bin${PATH:+:${PATH}}"; export PATH;
PERL5LIB="/home/gagbo/perl5/lib/perl5${PERL5LIB:+:${PERL5LIB}}"; export PERL5LIB;
PERL_LOCAL_LIB_ROOT="/home/gagbo/perl5${PERL_LOCAL_LIB_ROOT:+:${PERL_LOCAL_LIB_ROOT}}"; export PERL_LOCAL_LIB_ROOT;
PERL_MB_OPT="--install_base \"/home/gagbo/perl5\""; export PERL_MB_OPT;
PERL_MM_OPT="INSTALL_BASE=/home/gagbo/perl5"; export PERL_MM_OPT;
