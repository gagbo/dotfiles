# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
if test -r $HOME/miniconda3/bin/conda
    eval $HOME/miniconda3/bin/conda "shell.fish" "hook" $argv | source
end
# <<< conda initialize <<<

