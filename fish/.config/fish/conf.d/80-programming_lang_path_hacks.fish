# Programming Language env hacks
# This file is mainly about setting PATH correctly
# for all languages that ship with their own package manager

# Go
set -gx PATH $HOME/go/bin $PATH

# Javascript
set -gx PATH $HOME/.yarn/bin $PATH

# Lua
if command -q luarocks
  eval (luarocks path)
end

# Perl 5
set -gx PATH $HOME/perl5/bin $PATH
set -gx PERL5LIB $HOME/perl5/lib/perl5 $PERL5LIB
set -gx PERL_LOCAL_LIB_ROOT $HOME/perl5 $PERL_LOCAL_LIB_ROOT
set -gx PERL_MB_OPT "--install_base \"$HOME/perl5\""
set -gx PERL_MM_OPT "INSTALL_BASE=$HOME/perl5"

# Rust
set -gx PATH $HOME/.cargo/bin $PATH
