{-# OPTIONS_GHC -fno-warn-missing-signatures #-}

--------------------------------------------------------------------------------
-- | XMonad.hs
--
-- Gerry Agbobada's configuration file for xmonad using some of the latest recommended
-- features (e.g., 'desktopConfig').
module Main
  ( main
  )
where

--------------------------------------------------------------------------------
import           Control.Monad
import           System.Exit
import           XMonad
import           XMonad.Actions.GridSelect
import           XMonad.Actions.Volume
import           XMonad.Actions.Promote
import           XMonad.Actions.RotSlaves       ( rotSlavesDown
                                                , rotAllDown
                                                )
import           XMonad.Config.Desktop
import           XMonad.Hooks.DynamicLog
import           XMonad.Hooks.EwmhDesktops
import           XMonad.Hooks.FadeInactive      ( setOpacity )
import           XMonad.Hooks.ManageDocks
import           XMonad.Hooks.ManageHelpers
import           XMonad.Hooks.UrgencyHook
import           XMonad.Layout.BinarySpacePartition
                                                ( emptyBSP )
import           XMonad.Layout.Gaps
import           XMonad.Layout.Grid
import           XMonad.Layout.NoBorders        ( smartBorders )
import           XMonad.Layout.ResizableTile    ( ResizableTall(..) )
import           XMonad.Layout.Tabbed
import           XMonad.Layout.ToggleLayouts    ( ToggleLayout(..) )
import           XMonad.Prompt
import           XMonad.Prompt.ConfirmPrompt
import           XMonad.Prompt.Shell
import qualified XMonad.StackSet               as W
import           XMonad.Util.EZConfig
import           XMonad.Util.Run
import           XMonad.Util.NamedScratchpad
import           XMonad.Util.SpawnNamedPipe
import           XMonad.Util.SpawnOnce
import           XMonad.Util.Themes

--------------------------------------------------------------------------------
-- | Start xmonad using the main desktop configuration with
-- my overrides:
main :: IO ()
main =
  xmonad
    $                 withUrgencyHook NoUrgencyHook
    $                 desktopConfig
                        { terminal           = myTerminal
                        , modMask            = myModKey
                        , normalBorderColor  = "#0C0C14"
                        , focusedBorderColor = "#2DACB7"
                        , borderWidth        = 2
                        , startupHook        = myStartupHook
                        , handleEventHook = myEventHook <+> handleEventHook desktopConfig
                        , manageHook         = myManageHook
                        , workspaces         = myWorkspaces
                        , logHook = myLogHook "xmobtop" <+> logHook desktopConfig
                        , layoutHook         = myLayouts
                        }
    `additionalKeysP` myKeyBindings


--------------------------------------------------------------------------------
-- | Workspaces

-- Escape patterns for xmobar
xmobarEscape :: [Char] -> [Char]
xmobarEscape = concatMap doubleLts
 where
  doubleLts '<' = "<<"
  doubleLts x   = [x]

-- Workspaces definitions
workspaceNames = ["dev", "www", "sys", "doc", "5", "chat", "mus", "vid", "9"]
myWorkspaces :: [String]
-- myWorkspaces = [ show (n) ++ ":" ++ ws | (n, ws) <- zip [1 .. 9] workspaceNames]
myWorkspaces = clickable . (map xmobarEscape) $ workspaceNames
 where
  clickable l =
    [ "<action=`xdotool key super+" ++ show (n) ++ "`>" ++ ws ++ "</action>"
    | (i, ws) <- zip [1 .. 9] l
    , let n = i
    ]

--------------------------------------------------------------------------------
-- | Customize Startup
--
myStartupHook :: X ()
myStartupHook = do
  spawnOnce "dunst"
  spawnOnce "udiskie"
  spawnNamedPipe "xmobar ~/.config/xmobar/xmobarrc-top-only" "xmobtop"
  spawnOnce "nextcloud"

--------------------------------------------------------------------------------
-- | Customize the PrettyPrinting in Xmobar status bar
myLogHook :: String -> X ()
myLogHook pipeName = do
  mh <- getNamedPipe pipeName
  dynamicLogWithPP myXMobarPP { ppOutput = maybe (\_ -> return ()) hPutStrLn mh
                              }

myXMobarPP :: PP
myXMobarPP = xmobarPP { ppTitle = xmobarColor "#2DACB7" "" . shorten 50
                      , ppCurrent = xmobarColor "#E5AC39" "" . wrap "[" "]"
                      , ppHiddenNoWindows = xmobarColor "#2D2D4C" ""
                      , ppUrgent = xmobarColor "#E55C50" "#E5F4FF"
                      , ppHidden = xmobarColor "#82AAFF" "" . wrap "*" ""   -- Hidden workspaces in xmobar
                      }

--------------------------------------------------------------------------------
-- | Customize Events
--
myEventHook = fullscreenEventHook

------------------------------------------------------------------------
---GRID SELECT
------------------------------------------------------------------------

myColorizer :: Window -> Bool -> X (String, String)
myColorizer = colorRangeFromClassName (0x31, 0x2e, 0x39) -- lowest inactive bg
                                      (0x31, 0x2e, 0x39) -- highest inactive bg
                                      (0x61, 0x57, 0x72) -- active bg
                                      (0xc0, 0xa7, 0x9a) -- inactive fg
                                      (0xff, 0xff, 0xff) -- active fg

-- gridSelect menu layout
mygridConfig colorizer = (buildDefaultGSConfig colorizer) { gs_cellheight = 30
                                                          , gs_cellwidth = 200
                                                          , gs_cellpadding = 8
                                                          , gs_originFractX = 0.5
                                                          , gs_originFractY = 0.5
                                                          , gs_font = myFont
                                                          }

spawnSelected' :: [(String, String)] -> X ()
spawnSelected' lst = gridselect conf lst >>= flip whenJust spawn
  where conf = (mygridConfig defaultColorizer)

scratchpadGridSelect :: [(String, String)] -> X ()
scratchpadGridSelect lst = gridselect conf lst
  >>= flip whenJust (namedScratchpadAction scratchpads)
  where conf = (mygridConfig defaultColorizer)

--------------------------------------------------------------------------------
-- | Customize layouts.
--

-- myOuterGaps :: l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Gaps l a
-- myOuterGaps = gaps [(U, 10), (D, 10), (L, 10), (R, 10)]

-- myLayouts = (noBorders Full) ||| others
--  where
myLayouts =
  desktopLayoutModifiers
    $   smartBorders
    $   avoidStruts
    $   emptyBSP
    ||| tabbedAlways shrinkText (theme smallClean)
    ||| ResizableTall 1 (1.5 / 100) (toRational $ 2 / (1 + sqrt 5 :: Double)) []
    ||| Grid

--------------------------------------------------------------------------------
-- | Customize the way 'XMonad.Prompt' looks and behaves.  It's a
-- great replacement for dzen.
myXPConfig :: XPConfig
myXPConfig = def { position          = Top
                 , alwaysHighlight   = True
                 , promptBorderWidth = 1
                 , font              = myFont
                 , height            = 35
                 , bgColor           = "#1D1F21"
                 , fgColor           = "#CEDBE5"
                 , bgHLight          = "#DA9B1D"
                 }



--------------------------------------------------------------------------------
-- | Manipulate windows as they are created.  The list given to
-- @composeOne@ is processed from top to bottom.  The first matching
-- rule wins.
--
-- Use the `xprop' tool to get the info you need for these matches.
-- For className, use the second value that xprop gives you.
myManageHook :: ManageHook
myManageHook =
  manageDocks
    <+> myWindowRules
    <+> manageHook desktopConfig
    <+> myNSManageHook scratchpads


myWindowRules :: ManageHook
myWindowRules =
  composeAll
    . concat
    $ [ [isDialog --> doCenterFloat]
      , [isFullscreen --> doFullFloat]
      , [ (className =? c <||> title =? c <||> resource =? c) --> doIgnore
        | c <- bars
        ]
      , [ (className =? c <||> title =? c <||> resource =? c) --> doFloat
        | c <- nfloat
        ]
      , [ (className =? c <||> title =? c <||> resource =? c) --> doCenterFloat
        | c <- cfloat
        ]
      , [ (className =? c <||> title =? c <||> resource =? c)
            --> doShift (myWorkspaces !! 0)
        | c <- dev
        ]
      , [ (className =? c <||> title =? c <||> resource =? c)
            --> doShift (myWorkspaces !! 1)
        | c <- www
        ]
      , [ (className =? c <||> title =? c <||> resource =? c)
            --> doShift (myWorkspaces !! 2)
        | c <- sys
        ]
      , [ (className =? c <||> title =? c <||> resource =? c)
            --> doShift (myWorkspaces !! 3)
        | c <- doc
        ]
      , [ (className =? c <||> title =? c <||> resource =? c)
            --> doShift (myWorkspaces !! 4)
        | c <- ws5
        ]
      , [ (className =? c <||> title =? c <||> resource =? c)
            --> doShift (myWorkspaces !! 5)
        | c <- chat
        ]
      , [ (className =? c <||> title =? c <||> resource =? c)
            --> doShift (myWorkspaces !! 6)
        | c <- mus
        ]
      , [ (className =? c <||> title =? c <||> resource =? c)
            --> doShift (myWorkspaces !! 7)
        | c <- vid
        ]
      , [ (className =? c <||> title =? c <||> resource =? c)
            --> doShift (myWorkspaces !! 8)
        | c <- ws9
        ]
      , [ role =? c --> doFloat | c <- im ]     -- place roles on im
      ]
 where
  bars   = ["xmobar", "dzen2", "desktop_window"]
  nfloat = ["mpv", "vlc", "XCalc", "KeePass2", "KeePassXC", "Pidgin"]
  cfloat = ["MPCWindow", "HTOPWindow"]
  dev    = ["Emacs", "Gvim", "Nvim-qt"]
  www    = ["Firefox", "Chromium"]
  sys    = ["HTOPWindow"]
  doc    = []
  ws5    = ["KeePassXC"]
  chat   = ["Konversation", "Discord"]
  mus    = ["MPCWindow"]
  vid    = ["vlc"]
  ws9    = []
  im     = ["nothing"]
  role   = stringProperty "WM_WINDOW_ROLE"

-------------------------------------------------------------------------------
-- Additional key bindings / Keys definition
myKeyBindings :: [(String, X ())]
myKeyBindings =
  [ -- "Defaults" stolen from DistroTube
    ("M-S-q", confirmPrompt myXPConfig "exit" (io exitSuccess))
  , ("M-p"  , shellPrompt myXPConfig)
  , ( "M-b"
    , sendMessage ToggleStruts
    ) -- Make the windows go over status bars
  , ( "M-f"
    , sendMessage ToggleLayout
    ) -- Toggle between full screen and others
  , ("M-<Tab>"      , sendMessage NextLayout)
  , ("M-<Space>"    , spawn myRofi)
  , ("M-r"          , spawn myDMenu)
  , ("M-<Backspace>", focusUrgent)
  , ( "M-S-c"
    , kill
    )

        -- Xmonad
  , ( "M-q"
    , spawn "xmonad --recompile; xmonad --restart"
    )

        -- Windows navigation
  , ( "M-j"
    , windows W.focusDown
    )               -- Move focus to the next window
  , ( "M-k"
    , windows W.focusUp
    )                 -- Move focus to the prev window
  , ( "M-S-m"
    , windows W.swapMaster
    )            -- Swap the focused window and the master window
  , ( "M-S-j"
    , windows W.swapDown
    )              -- Swap the focused window with the next window
  , ( "M-S-k"
    , windows W.swapUp
    )                -- Swap the focused window with the prev window
  , ( "M-S-<Backspace>"
    , promote
    )                  -- Moves focused window to master, all others maintain order
  , ( "M1-S-<Tab>"
    , rotSlavesDown
    )              -- Rotate all windows except master and keep focus in place
  , ( "M1-C-<Tab>"
    , rotAllDown
    )                 -- Rotate all the windows in the current stack

        -- Gaps Control
  , ( "M-C-g"
    , sendMessage ToggleGaps
    )

        -- Lock screen
  , ( "M-S-z"
    , spawn myScreenLock
    )

        -- Terminal apps
  , ("M-<Return>", spawn myTerminal)
  , ("M-i", spawn $ myTerminal ++ " --class HTOPWindow -e " ++ myConsoleInfo)
  , ( "M-o"
    , spawn $ myTerminal ++ " --class IRCWindow -e " ++ myConsoleIRCClient
    )

        -- Scratchpads
  , ("<F12>", namedScratchpadAction scratchpads "term")
  , ("M-C-s", namedScratchpadAction scratchpads "volume")
  , ( "<F10>"
    , scratchpadGridSelect
      [ ("Terminal" , "term")
      , ("Music"    , "music")
      , ("Volume"   , "volume")
      , ("KeePass"  , "KeePass")
      , ("NextCloud", "Nextcloud")
      ]
    )

        --- Grid Select
  , ( "M-S-t"
    , spawnSelected'
      [ ("Settings", "systemsettings5")
      , ("Kitty"   , "kitty")
      , ("Emacs"   , "emacs")
      , ("Firefox" , "firefox")
      , ("Kitty"   , "kitty")
      , ("Discord" , "Discord")
      , ("Keepass" , "keepassxc")
      ]
    )
  , ("M-S-g", goToSelected $ mygridConfig myColorizer)
  , ( "M-S-b"
    , bringSelected $ mygridConfig myColorizer
    )


        -- Brightness Control
  , ("<XF86MonBrightnessUp>", spawn "brightnessctl -d intel_backlight set 5%+")
  , ( "<XF86MonBrightnessDown>"
    , spawn "brightnessctl -d intel_backlight set 5%-"
    )

        -- Media keys
  , ("<XF86AudioLowerVolume>", setMute True >> lowerVolume 4 >> return ())
  , ("<XF86AudioRaiseVolume>", setMute True >> raiseVolume 4 >> return ())
  , ("<XF86AudioMute>"       , Control.Monad.void toggleMute)
  , ("<XF86AudioPlay>"       , spawn "mpc -q toggle")
  , ("<XF86AudioPause>"      , spawn "mpc -q pause")
  , ("<XF86AudioStop>"       , spawn "mpc -q stop")
  , ("<XF86AudioPrev>"       , spawn "mpc -q prev")
  , ("<XF86AudioNext>"       , spawn "mpc -q next")
  , ( "<XF86Music>"
    , spawn $ myTerminal ++ " --class MPCWindow -e " ++ myConsoleMusicPlayer
    )
  , ( "M-m"
    , spawn $ myTerminal ++ " --class MPCWindow -e " ++ myConsoleMusicPlayer
    )
  ]

-------------------------------------------------------------------------------
-- Scratchpads
scratchpads :: [NamedScratchpad]
scratchpads =
  [ NS "music"
       "kitty --title Music -e ncmpcpp"
       (title =? "Music")
       (customFloating $ W.RationalRect (1 / 6) (1 / 20) (2 / 3) (2 / 3))
  , NS "term"
       "kitty --title Scratchpad"
       (title =? "Scratchpad")
       (customFloating $ W.RationalRect (1 / 12) (1 / 20) (5 / 6) (2 / 3))
  , NS "volume"
       "pavucontrol"
       (className =? "Pavucontrol")
       (customFloating $ W.RationalRect (1 / 4) (1 / 20) (2 / 4) (2 / 4))
  , NS "KeePass"
       "keepassxc"
       (className =? "KeePassXC")
       (customFloating $ W.RationalRect (1 / 12) (1 / 20) (5 / 6) (2 / 3))
  , NS "Nextcloud"
       "nextcloud"
       (className =? "Nextcloud")
       (customFloating $ W.RationalRect (1 / 12) (1 / 20) (5 / 6) (2 / 3))
  ]

myNSManageHook :: NamedScratchpads -> ManageHook
myNSManageHook s = namedScratchpadManageHook s <+> composeOne
  [ title =? "Music" -?> (ask >>= \w -> liftX (setOpacity w 0.7) >> idHook)
  , title =? "Scratchpad" -?> (ask >>= \w -> liftX (setOpacity w 0.8) >> idHook)
  , className
  =?  "Pavucontrol"
  -?> (ask >>= \w -> liftX (setOpacity w 0.8) >> idHook)
  ]

-------------------------------------------------------------------------------
-- Local programs
myTerminal :: String
myTerminal = "kitty"
myModKey :: KeyMask
myModKey = mod4Mask -- Use the "Win" key for the mod key
myConsoleMusicPlayer :: String
myConsoleMusicPlayer = "ncmpcpp"
myConsoleInfo :: String
myConsoleInfo = "htop"
myConsoleIRCClient :: String
myConsoleIRCClient = "weechat"
-- myScreenCap :: String
-- myScreenCap = "spectacle"
myDMenu :: String
myDMenu = "dmenu_run -h 35"
myRofi :: String
myRofi = "bash ~/bin/rofiki"
myScreenLock :: String
myScreenLock = "xset s activate"
myFont :: String
myFont = "xft:Hack:style=Regular:size=12"
