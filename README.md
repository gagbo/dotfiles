# Dotfiles

Everything is managed by stow and makes use of stowrc and stowignore dotfiles
to be as smooth as possible

## TODO

### Meta

- [ ] Remove the org README
- [ ] Add how-to deploy

### Configs

- [ ] Make git/config easier to use on other hosts
